from django.contrib import admin
from .models import *


class MovimientoAdmin(admin.ModelAdmin):
    model = Movimiento

    list_display = (
        'fecha',
        'articulo',
        'tipo_movimiento',
        'cantidad',
    )


admin.site.register(Movimiento, MovimientoAdmin)
admin.site.register(Tipo_movimiento)
