# Generated by Django 2.2 on 2019-04-05 15:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articulo',
            name='descripcion',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='Descripción'),
        ),
    ]
