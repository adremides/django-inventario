from django.db import models


class Articulo(models.Model):
    codigo = models.CharField('Código', max_length=13, unique=True)
    nombre = models.CharField(max_length=25, unique=True)
    descripcion = models.CharField('Descripción', max_length=60, blank=True, null=True)
    medida = models.ForeignKey('productos.medida', on_delete=models.PROTECT)
    envase = models.ForeignKey('productos.envase', on_delete=models.PROTECT)
    ultimo_precio_lista = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)

    class Meta:
        verbose_name = 'Artículo'
        verbose_name_plural = 'Artículos'

    def __str__(self):
        return self.nombre


class Medida(models.Model):
    nombre = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.nombre


class Envase(models.Model):
    nombre = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.nombre
